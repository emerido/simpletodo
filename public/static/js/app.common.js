angular.module('todo.common', [])

    .filter('moment', function() {
        return function(input, format, lang) {
            lang    = lang || 'ru';
            input   = input || '';

            var date = moment(input).lang(lang);

            if (false === date.isValid()) {
                date.setDate(new Date())
            }
            return date.format(format);
        };
    })

    .factory('$date', [function() {
        var months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

        var parsers = {
            'yesterday' : {
                'pattern' : 'завтра',
                'callback' : function(moment) {
                    return moment.add('day', 1);
                }
            },
            'tomorrow' : {
                'pattern' : 'вчера',
                'callback' : function(moment) {
                    return moment.subtract('day', 1);
                }
            },
            'date'  : {
                'pattern'   : '\\d+\\s[а-я]+',
                'callback'  : function(moment, match) {
                    if (match[0]) {
                        var parts = match[0].split(' ');

                        var day = parseInt(parts[0]);
                        if (day) {
                            moment = moment.date(day);
                        }

                        var month = months.indexOf(angular.lowercase(parts[1]));
                        if (month !== -1) {
                            moment = moment.month(month);
                        }
                    }
                    return moment;
                }
            }

        }

        return function(text) {

            var match = null;
            var result = moment();

            angular.forEach(parsers, function(parser, name) {
                if (parser.pattern.constructor !== RegExp) {
                    parser.pattern = new RegExp(parser.pattern, 'i');
                }
                match = parser.pattern.exec(text);

                if (match) {
                    result = parser.callback(result, match);
                }
            });

            return result.toDate();
        }
    }])
;