angular.module('todo', ['todo.common'])

    .controller('TodoCtrl', ['$scope', '$filter', '$http', '$date', '$interval', function($scope, $filter, $http, $date, $interval) {

        $scope.init = false;

        /**
         * New item text
         *
         * @type {string}
         */
        $scope.text = '';

        $scope.dateUpdated = new Date();

        /**
         * Todo items
         *
         * @type {Array}
         */
        $scope.items = [];

        /**
         * Todo items order
         *
         * @type {boolean}
         */
        $scope.itemsReverse = true;

        /**
         * Toggle item done status
         *
         * @param item
         */
        $scope.toggleItem = function(item) {
            item.done = !item.done;
            $scope.updateItem(item);
        };

        /**
         * Update item info
         *
         * @param item
         */
        $scope.updateItem = function(item) {
            $http.post('/tasks/update', item);
        };

        /**
         * Remove item
         *
         * @param item
         */
        $scope.removeItem = function(item) {
            var index = $scope.items.indexOf(item);
            if (index > -1) {
                $scope.items.splice(index, 1);
            }
            $http.post('/tasks/remove', item);
        };

        $scope.createItem = function() {
            if ($scope.text.length == 0) {
                return;
            }

            var item = {
                'text'  : $scope.text,
                'date'  : $date($scope.text),
                'done'  : false
            }

            $scope.text = '';

            $http.post('/tasks/create', item).success(function(data, status) {
                if (status === 200 && data.success) {
                    item = data.record;
                    $scope.lastItemId = item.id;
                }
                $scope.items.push(item);
            });


        };

        $scope.refreshItems = function() {
            $http.post('/tasks/new', {'date' : $scope.dateUpdated}).success(function(data, status) {
                $scope.dateUpdated = new Date();
                if (status === 200) {
                    angular.forEach(data, function(item) {

                        // Search exists item
                        var result = $.grep($scope.items, function(e) {
                            return e.id == item.id;
                        });

                        var exists = result.shift();

                        item.done = item.done === '1';

                        if (exists) {
                            exists.done = item.done;
                        } else {
                            $scope.items.push(item);
                        }
                    })
                }
            });
        };

        $scope.activeItems = function() {
            return $.grep($scope.items, function(item) {
                return !item.done;
            });
        };

        $scope.finishedItems = function() {
            return $.grep($scope.items, function(item) {
                return item.done;
            });
        };

        $scope.isDeadline = function(item) {
            return new Date(item.date) <= new Date();
        }

        $http.get('/tasks').success(function(data, status) {

            if (status === 200 && data.length) {
                data = $.map(data, function(item) {
                    item.done = item.done === '1';
                    if (item.id > $scope.lastItemId) {
                        $scope.lastItemId = item.id;
                    }
                    return item
                });

                $scope.items = data;
            }
        });

        $interval(function() {
            $scope.refreshItems()
        }, 400);
    }])
;