<?php require __DIR__ . '/../vendor/autoload.php';

date_default_timezone_set('Europe/Moscow');

$app = new \Slim\Slim();

$app->add(new App\JsonRequest());
/**
 * Create database service
 *
 * @return PDO
 */
$app->db = function () {
    return new PDO('mysql:host=localhost;port=3306;dbname=todo', 'todo', null, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
};

/**
 * Setup default configuretion
 */
$app->config([
    'templates.path'    => __DIR__ . '/views'
]);


/**
 * Render main page
 */
$app->get('/', function () use ($app) {
    $app->render('index.php');
});

/**
 * Returns all tasks from database
 */
$app->get('/tasks', function() use ($app) {
    /** @var PDO $db */
    $db = $app->db;

    $tasks = [];


    $query = $db->query('SELECT * FROM tasks');

    if ($query->rowCount()) {
        $tasks = $query->fetchAll(PDO::FETCH_ASSOC);
    }

    $app->response->header('Content-Type', 'application/json');
    $app->response->body(json_encode($tasks));
});


/**
 * Create new task
 */
$app->post('/tasks/create', function() use ($app) {
    /** @var PDO $db */
    $db = $app->db;

    /** @var stdClass $data */
    $data = $app->request->getBody();

    $query = $db->prepare('INSERT INTO `tasks` (`text`, `date`, `updated`) VALUES (:text, :date, :updated)');

    $result = $query->execute([
        ':text'    => $data->text,
        ':date'    => $data->date,
        ':updated' => date('Y-m-d H:m:s', time()),
    ]);

    if ($result) {
        $data->id = $db->lastInsertId();
    }

    $response = [
        'success'   => $result,
        'record'    => $data
    ];

    $app->response->header('Content-Type', 'application/json');
    $app->response->body(json_encode($response));
});

/**
 * Update exists task
 */
$app->post('/tasks/update', function() use ($app) {
    /** @var PDO $db */
    $db = $app->db;

    /** @var stdClass $data */
    $data = $app->request->getBody();

    $query = $db->prepare('UPDATE tasks SET text=:text, date=:date, done=:done, updated=:updated WHERE id=:id');

    $result = $query->execute([
        ':id'      => $data->id,
        ':text'    => $data->text,
        ':done'    => $data->done,
        ':date'    => $data->date,
        ':updated' => date('Y-m-d H:m:s', time()),
    ]);

    $response = [
        'success'   => $result,
        'record'    => $data
    ];

    $app->response->header('Content-Type', 'application/json');
    $app->response->body(json_encode($response));
});

/**
 * Remove task
 */
$app->post('/tasks/remove', function() use ($app) {

    /** @var PDO $db */
    $db = $app->db;

    /** @var stdClass $data */
    $data = $app->request->getBody();

    $query = $db->prepare('DELETE FROM tasks WHERE id=:id');

    $result = $query->execute([
        ':id'      => $data->id,
    ]);

    $response = [
        'success'   => $result,
        'record'    => $data
    ];

    $app->response->header('Content-Type', 'application/json');
    $app->response->body(json_encode($response));
});


/**
 * Returns all tasks from database
 */
$app->post('/tasks/new', function() use ($app) {
    /** @var PDO $db */
    $db = $app->db;

    /** @var stdClass $data */
    $data = $app->request->getBody();

    $tasks = [];

    $query = $db->prepare('SELECT * FROM tasks WHERE updated > :date');
    $query->bindParam(':date', $data->date);

    if ($query->execute()) {
        $tasks = $query->fetchAll(PDO::FETCH_ASSOC);
    }

    $app->response->header('Content-Type', 'application/json');
    $app->response->body(json_encode($tasks));
});

$app->run();