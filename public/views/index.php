<!DOCTYPE html>
<html ng-app="todo">
<head>
    <meta charset="utf-8">
    <title>Simple TODO</title>
    <link rel="stylesheet" href="/static/css/bootstrap.css">
    <link rel="stylesheet" href="/static/css/style.css">
</head>
<body>

    <div class="container" ng-controller="TodoCtrl">

        <h2 class="todo-title">TODO</h2>

        <div class="col-md-6 col-md-offset-3">

            <div class="todo-input">
                <form ng-submit="createItem()">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Создайте новую задачу" ng-model="text">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" ng-click="createItem()">Добавить</button>
                    </span>
                </div>
                </form>
            </div>

            <div class="todo-count">
                Активных задач: {{activeItems().length}}. Завершенных задач: {{finishedItems().length}}
            </div>

            <div class="todo-items">
                <ul>
                    <li ng-repeat="item in items | orderBy:'date':true" class="todo-item todo-item-done-{{item.done}}" ng-class="{'todo-item-deadline' : isDeadline(item)}">
                        <span class="todo-item-btns btn-group">
                            <button class="btn btn-xs btn-default" ng-class="{'btn-success' : item.done}" type="button" ng-click="toggleItem(item)"><i class="glyphicon glyphicon-ok"></i></button>
                            <button class="btn btn-xs btn-danger" type="button" ng-click="removeItem(item)"><i class="glyphicon glyphicon-trash"></i></button>
                        </span>
                        <span class="todo-item-date">{{item.date | moment: 'DD MMMM'}}</span>
                        <span class="todo-item-text">{{item.text}}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <script src="/static/js/libs/jquery.min.js"></script>
    <script src="/static/js/libs/moment.js"></script>
    <script src="/static/js/libs/angular.min.js"></script>
    <script src="/static/js/libs/bootstrap.min.js"></script>
    <script src="/static/js/app.common.js"></script>
    <script src="/static/js/app.js"></script>

</body>
</html>
