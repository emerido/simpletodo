<?php

namespace App
{


    use Slim\Middleware;

    class JsonRequest extends Middleware
    {

        /**
         * Call
         *
         * Perform actions specific to this middleware and optionally
         * call the next downstream middleware.
         */
        public function call()
        {
            $body = $this->app->request->getBody();

            if ($body = json_decode($body)) {
                $this->app->environment['slim.input'] = $body;
            }

            $this->next->call();
        }

    }

}